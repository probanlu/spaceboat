const fs = require('fs');
const Discord = require('discord.js');
const utils = require('./utils');
const config = require('./data/config.json');

let mutes;

try {
    mutes = require('./data/config.json');
}
catch (error) {
    console.log(`${error.message}\nPlease edit the config-example.json file, and rename it as config.json!`);
}

try {
    mutes = require('./data/mutes.json');
}
catch (error) {
    console.log(`${error.message}`);
    console.log(`Creating new mutes.json file.`);
    try {
        fs.writeFileSync("./data/mutes.json", JSON.stringify({}, null, 4));
    } catch (err) {
        console.error('Error creating mutes.json file:', err);
    }
    mutes = require('./data/mutes.json');
}
try {
    var bans = require('./data/bans.json');
}
catch (error) {
    console.log(`${error.message}`);
    console.log(`Creating new bans.json file.`);
    try {
        fs.writeFileSync("./data/bans.json", JSON.stringify({}, null, 4));
    } catch (err) {
        console.error('Error creating bans.json file:', err);
    }
    var bans = require('./data/bans.json');
}
try {
    var suspendrolename = require('./data/suspendrolename.json'); // I know this is the worst way to do this, but I'm gunna be rewriting soon-ish anyway so hush
}
catch (error) {
    console.log(`${error.message}`);
    console.log(`Creating new suspendrolename.json file.`);
    try {
        fs.writeFileSync("./data/suspendrolename.json", JSON.stringify({}, null, 4));
    } catch (err) {
        console.error('Error creating suspendrolename.json file:', err);
    }
    var suspendrolename = require('./data/suspendrolename.json');
}

try {
    var guilds = require('./data/guilds.json');
}
catch (error) {
    console.log(`${error.message}\nCreating new guilds.json file.`);
    try {
        fs.writeFileSync("./data/guilds.json", JSON.stringify({}, null, 4));
    } catch (err) {
        console.error('Error saving guilds.json file:', err);
    }
    var guilds = require('./data/guilds.json');
}

try {
    var rolereact = require('./data/rolereact.json');
}
catch (error) {
    console.log(`${error.message}\nCreating new rolereact.json file.`);
    try {
        fs.writeFileSync("./data/rolereact.json", JSON.stringify({}, null, 4));
    } catch (err) {
        console.error('Error saving rolereact.json file:', err);
    }
    var rolereact = require('./data/rolereact.json');
}

const bot = new Discord.Client({
    ws: {
        intents: new Discord.Intents(
            Discord.Intents.FLAGS.GUILDS |
            Discord.Intents.FLAGS.GUILD_MEMBERS |
            Discord.Intents.FLAGS.GUILD_MESSAGES |
            Discord.Intents.FLAGS.GUILD_MESSAGE_REACTIONS),
    },
});

const botCommands = new Discord.Collection();

fs.readdir("./cmds/", (err, files) => {
    if (err) console.error(err);

    let jsfiles = files.filter(f => f.split(".").pop() === "js")
    if (jsfiles.length <= 0) {
        console.log("No commands to load!");
        return;
    }

    console.log(`loading ${jsfiles.length} commands!`);

    jsfiles.forEach((f, i) => {
        let props = require(`./cmds/${f}`)
        console.log(`${i + 1}: ${f} loaded!`)
        botCommands.set(props.help.name, props);
    });
});

bot.on('ready', () => {
    console.log(`${bot.user.username} switched on.`);

    let botGuildsIDs = Array.from(bot.guilds.cache.keys());
    for (let i in botGuildsIDs) {
        if (!guilds.hasOwnProperty(botGuildsIDs[i])) {
            console.log(`Setting up ${botGuildsIDs[i]}`);
            guilds[botGuildsIDs[i]] = {
                logChannelID: '',
                botChannelID: '',
                adminbotChannelID: '',
            }
            fs.writeFile("./data/guilds.json", JSON.stringify(guilds, null, 4), err => {
                if (err) console.error('Error saving guilds.json file:', err);
            });
            console.log("Guild added to guilds.json. Please configure channelIDs.");
        }
    }

    for (let i in guilds) {
        let guild = bot.guilds.cache.get(i);
        if (!guild) {
            console.log(`Error: Guild not found! Removing from records...`);
            guilds[i] = null;
            delete guilds[i];
            fs.writeFile("./data/guilds.json", JSON.stringify(guilds, null, 4), err => {
                if (err) console.error('Error saving guilds.json file: ', err);
            });
            console.log(`Removed successfully!`);
            continue;
        }
        if (!guilds[i].logChannelID) {
            console.log(`ERROR: Server [${guild.name}] does not have a logging channel!`)
        }
        if (!guilds[i].botChannelID) {
            console.log(`ERROR: Server [${guild.name}] does not have a specified bot channel!`)
        }
        if (!guilds[i].adminbotChannelID) {
            console.log(`ERROR: Server [${guild.name}] does not have an admin bot channel!`)
        }
        console.log(`Server [${guild.name}] loaded with ${guild.channels.cache.size} channels and ${guild.memberCount} members.`);
    }
    if (mutes) {
        bot.setInterval(() => {
            for (let i in mutes) {
                let time = mutes[i].time;
                let guildID = mutes[i].guild;
                let guild = bot.guilds.cache.get(guildID);
                let member = guild.members.cache.get(i);
                let mutedRole = guild.roles.cache.find(r => r.name === "Muted");
                // check if member is still in server
                if (!member) {
                    console.log('ERROR: User is not in the server anymore!');
                    delete mutes[i];
                    try {
                        fs.writeFileSync("./data/mutes.json", JSON.stringify(mutes, null, 4));
                    } catch (err) {
                        console.error('Error saving mutes.json file: ', err);
                    }
                    continue;
                }
                // check if the member has the muted role.
                if (!member.roles.cache.has(mutedRole.id)) {
                    console.log('User has been manually unmuted!');
                    delete mutes[i];
                    try {
                        fs.writeFileSync("./data/mutes.json", JSON.stringify(mutes, null, 4));
                    } catch (err) {
                        console.error('Error saving mutes.json file: ', err);
                    }
                    let logChannel = guild.channels.cache.get(guilds[guildID].logChannelID);
                    try {
                        logChannel.send({
                            embed: new Discord.MessageEmbed()
                                .setDescription(`${member} was unmuted manually before the end of term.`)
                                .setFooter(`ID: ${member.id}`)
                                .setAuthor(`Member was unmuted.`, member.user.displayAvatarURL())
                                .setTimestamp()
                                .setColor(utils.colours.green),
                        });
                    }
                    catch (error) {
                        if (!logChannel) console.log('No logchannel defined for this guild!');
                        else console.log(error);
                    }
                    continue;
                }
                // automatic unmute
                if (Date.now() > time) {
                    // unmute
                    member.roles.remove(mutedRole);
                    // notify console
                    console.log(`${member.user.username} has been unmuted.`);
                    // notify logchannel
                    let logChannel = guild.channels.cache.get(guilds[guildID].logChannelID);
                    utils.logChannel(bot, guildID, utils.colours.green, `Member unmuted.`, member.user, bot.user, `Automatic.`);
                    //remove the entry
                    mutes[i] = null;
                    delete mutes[i];
                    try {
                        fs.writeFileSync("./data/mutes.json", JSON.stringify(mutes, null, 4));
                    } catch (err) {
                        console.error('Error saving mutes.json file:', err);
                    }
                }
            }
        }, 5000);
    }
    if (bans) {
        bot.setInterval(async () => {
            let guildBansByGuildID;
            try {
                guildBansByGuildID = await Promise.all(
                    [...new Set(Object.values(bans).map(ban => ban.guild))].map(id => bot.guilds.cache.get(id).fetchBans()));
            } catch (err) {
                console.error('Error fetching guild bans on auto-unban interval:', err);
            }
            for (const j in bans) {
                const time = bans[j].time;
                const guildID = bans[j].guild;
                const guild = bot.guilds.cache.get(guildID);
                const guildBans = guildBansByGuildID[guildID];
                if (!(guildBans && guildBans.has(j))) continue;
                const { user } = guildBans.get(j);
                if (Date.now() > time) {
                    if (user) {
                        guild.members.unban(user, `Automatic: Tempban term ended.`);
                        try {
                            delete bans[j];
                            fs.writeFile("./data/bans.json", JSON.stringify(bans, null, 4), err => {
                                if (err) console.error('Error saving bans.json file:', err);
                            });
                        } catch (error) { console.log(error) }
                        let logChannel = guild.channels.cache.get(guilds[guildID].logChannelID);
                        console.log(`${user.username} has been unbanned.`);
                        utils.logChannel(bot, guildID, utils.colours.green, `Member unbanned.`, user, bot.user, `Automatically unbanned - Temporary ban term ended.`);
                    } else {
                        console.log(`User ${j} not found!`);
                        try {
                            guild.channels.cache.get(guilds[guildID].logChannelID).send({
                                embed: new Discord.MessageEmbed()
                                    .setDescription(`${user.username} was unbanned manually before the end of term.`)
                                    .setFooter(`ID: ${user.id}`)
                                    .setAuthor(`Member was unbanned.`, user.displayAvatarURL())
                                    .setTimestamp()
                                    .setColor(utils.colours.green),
                            });
                        } catch (error) {
                            // if (!logChannel) console.log('No logchannel defined for this guild!');
                            console.log(error);
                        };
                        try {
                            delete bans[j];
                            fs.writeFile("./data/bans.json", JSON.stringify(bans, null, 4), err => {
                                if (err) console.error('Error saving bans.json file:', err);
                            });
                        }
                        catch (error) { console.log(error) }
                    }
                }
            }
        }, 30000);
    }
});

bot.on('message', message => {
    if (message.author.bot) return;
    if (message.mentions.everyone || message.mentions.roles.first()) {
        // don't log mentions that happen in channels that are in admin or modmail channel categories
        const channelCategoryName = message.channel.parent ? message.channel.parent.name.replace(/\s+/g, '').toLowerCase() : '';
        if (!(channelCategoryName == 'admin' || channelCategoryName == 'modmail')) {
            utils.logChannel(bot, message.guild.id, utils.colours.purple, 'Mass ping!', message.author, '', `sent message: "${message}"`, '')
        }
    }
    if (!(message.channel.type === "text")) return;

    let messageArray = message.content.split(/\s+/g);
    let command = messageArray[0];
    let args = messageArray.slice(1);

    if (!command.startsWith(config.prefix)) return;

    let cmd = botCommands.get(command.slice(config.prefix.length));
    if (cmd) cmd.run(bot, message, args);
});

bot.on('messageReactionAdd', (messageReaction, user) => {
    if (rolereact[messageReaction.message.id]) {
        let message = messageReaction.message;
        let emoji = rolereact[message.id].emoji;
        if (emoji[0] == '<') {
            let emojiID = emoji.split(':')[2].split('>')[0]; //custom emoji ID.
            //console.log(emojiID);
            emoji = bot.emojis.cache.get(emojiID)
        }
        let role = message.guild.roles.cache.get(rolereact[message.id].role);
        if (emoji == messageReaction.emoji) {
            messageReaction.users.cache.forEach(user => {
                if (user === bot.user) return;
                try {
                    message.guild.member(user).roles.add(role, "Automated Reaction Role in #" + message.channel.name);
                    messageReaction.users.remove(user);
                } catch (err) {
                    if (err.name == "TypeError: Cannot read property 'roles' of null") {
                        try {
                            console.log("Couldn't find " + user.username + " in the members list, trying alternative method.")
                            message.guild.members.cache.get(user.id).roles.add(role, "Automated Reaction Role in #" + message.channel.name);
                            messageReaction.users.remove(user);
                        } catch (err) { console.log(err) }
                    } else console.log(err)
                }
                console.log("Role added to " + user.username + "#" + user.discriminator + " from message in #" + message.channel.name);
            });
        }
    }
});

bot.on('guildMemberAdd', member => {
    let logChannel = member.guild.channels.cache.get(guilds[member.guild.id].logChannelID);
    try {
        logChannel.send({
            embed: new Discord.MessageEmbed()
                .setThumbnail(member.user.displayAvatarURL())
                .setDescription(`${member} - ${member.user.tag}`)
                .setFooter(`ID: ${member.id}`)
                .setAuthor(`Member joined!`, member.user.displayAvatarURL())
                .setTimestamp()
                .setColor(utils.colours.blue),
        });
    } catch (error) {
        if (!logChannel) console.log('No logchannel defined for this guild!');
        else console.log(error);
    }
    console.log(`Member joined! ${member.user.tag}`);
});

//below is repeat-logged when member kicked/banned. Must fix when warnings introduced.

bot.on('guildMemberRemove', member => {
    let logChannel = member.guild.channels.cache.get(guilds[member.guild.id].logChannelID)
    try {
        logChannel.send({
            embed: new Discord.MessageEmbed()
                .setThumbnail(member.user.displayAvatarURL())
                .setDescription(`${member} - ${member.user.tag}`)
                .setFooter(`ID: ${member.id}`)
                .setAuthor(`Member left.`, member.user.displayAvatarURL())
                .setTimestamp()
                .setColor(utils.colours.orange),
        });
    } catch (error) {
        if (!logChannel) console.log('No logchannel defined for this guild!');
        else console.log(error);
    }
    console.log(`Member left! ${member.user.tag}`);
});

bot.on('guildCreate', guild => {
    guilds[guild.id] = {
        logChannelID: '',
        botChannelID: '',
        adminbotChannelID: '',
    };
    fs.writeFile("./data/guilds.json", JSON.stringify(guilds, null, 4), err => {
        if (err) console.error('Error saving guilds.json file:', err);
    });
    console.log(`Joined new server! Please set up Channel IDs.`)
});

bot.on('messageDelete', message => {
    if (!message.guild || !message.guild.channels || (!message.cleanContent && !message.attachments.first())) {
        return;
    };

    let channel = message.guild.channels.cache.find(c => c.name === "adminlog");
    if (!channel) return;

    let embed = new Discord.MessageEmbed()
        .setTitle('Message Deleted!')
        .setDescription(`\`\`\`\n${(message.cleanContent).substr(0, 1950)}\n\`\`\``)
        .addField('Channel', `${message.channel}`, true)
        .addField('Link to Context', `[JUMP](https://discord.com/channels/${message.guild.id}/${message.channel.id}/${message.id})`, true)
        .setColor('red')
        .setTimestamp(new Date())
        .setFooter(`Author: @${message.author.username}#${message.author.discriminator}`, message.author.avatarURL())
        .setColor(utils.colours.darkblue);

    if (message.attachments.size > 0) {
        const attachment = message.attachments.first();
        if (attachment.width) {
            embed.setImage(attachment.url);
        } else {
            embed.attachFiles([attachment.url]);
        }
        if (message.cleanContent) {
            embed.setDescription(`\`\`\`\n${(message.cleanContent).substr(0, 1950)}\n\`\`\`\n[File](${attachment.url})`);
        } else embed.setDescription(`[File](${attachment.url})`);
    } else if (message.cleanContent) embed.setDescription(`\`\`\`\n${(message.cleanContent).substr(0, 1750)}\n\`\`\``);

    channel.send({ embed });
});

bot.on('messageDeleteBulk', messages => {

    messages.each(message => {
        if (!message.guild || !message.guild.channels || (!message.cleanContent && !message.attachments.first())) {
            return;
        };
        let channel = message.guild.channels.cache.find(c => c.name === "adminlog");
        if (!channel) return;

        let embed = new Discord.MessageEmbed()
            .setTitle('Message Deleted! (in bulk)')
            .setDescription(`\`\`\`\n${(message.cleanContent).substr(0, 1950)}\n\`\`\``)
            .addField('Channel', `${message.channel}`)
            .setColor(utils.colours.purple)
            .setTimestamp(new Date())
            .setFooter(`Author: @${message.author.username}#${message.author.discriminator}`, message.author.avatarURL());

        if (message.attachments.size > 0) {
            const attachment = message.attachments.first();
            if (attachment.width) {
                embed.setImage(attachment.url);
            } else {
                embed.attachFiles([attachment.url]);
            }
            if (message.cleanContent) {
                embed.setDescription(`\`\`\`\n${(message.cleanContent).substr(0, 1950)}\n\`\`\`\n[File](${attachment.url})`);
            } else embed.setDescription(`[File](${attachment.url})`);
        } else if (message.cleanContent) embed.setDescription(`\`\`\`\n${(message.cleanContent).substr(0, 1750)}\n\`\`\``);

        channel.send({ embed });
    });
});

// please fix: the below will activate even if user has been banned by bot. Will result in multiple logs.

// bot.on('guildBanAdd', guild, user => {
//     let logChannel = guild.channels.cache.get(guilds[guild.id].logChannelID)
//     try {
//         logChannel.send({
//             embed: new Discord.MessageEmbed()
//                 .setDescription(`**User:** ${user}\n`)
//                 .setFooter(`ID: ${target.id}`)
//                 .setAuthor(`Member Banned!`, target.user.displayAvatarURL())
//                 .setTimestamp()
//         })
//     }
//     catch (error) {
//         if (!logChannel) console.log('No logchannel defined for this guild!');
//         else console.log(error);
//     }
// });

//same for below.

// bot.on('guildBanRemove', guild, user => {
//     let logChannel = guild.channels.cache.get(guilds[guild.id].logChannelID)
//     try {
//         logChannel.send({
//             embed: new Discord.MessageEmbed()
//                 .setDescription(`**User:** ${user}\n*User was unbanned manually.*`)
//                 .setFooter(`ID: ${target.id}`)
//                 .setAuthor(`Member Unbanned.`, target.user.displayAvatarURL())
//                 .setTimestamp()
//         })
//     }
//     catch (error) {
//         if (!logChannel) console.log('No logchannel defined for this guild!');
//         else console.log(error);
//     }
// remove ban length from json
// });

bot.on('error', error => {
    console.log(`Bot has been disconnected with an error!\n${error}`);
});
bot.on('disconnect', event => {
    console.log(`Disconnected!\n${event.reason}`);
});
bot.on('destroy', () => {
    utils.db.close((err) => {
        if (err) return console.error(err.message);
        else console.log('Closed database connection.');
    });
});
bot.login(config.token)
    .then(() => console.log('Successful Login.'))
    .catch(error => {
        console.log(`Login unsuccessful!\n${error}`);
    });

module.exports = bot;
