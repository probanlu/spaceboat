const utils = require('../utils');

module.exports.run = async (bot, message, args) => {
    const { guild, author } = message;
    const warns = await utils.getWarns(guild.id, author.id).catch(err => console.log(err));
    const embedDesc = `Warning Severity Tally: ${utils.getWarnsTally(warns)}`;
    const embedFields = warns.map(warn => ({
        name: `**Warn ID:** ${warn.warn_id}`,
        value: `\n**Severity:** ${warn.severity}\n**Mod:** ${guild.members.cache.get(warn.moderator_id)}\n**Warning:** "${warn.warn_str}"\n**Date:** ${(new Date(warn.date)).toDateString()}\n`,
        inline: true,
    }));
    await utils.sendPagedEmbeds(author, '', embed => embed.setAuthor(author.username, author.displayAvatarURL()).setDescription(embedDesc), embedFields);
};

module.exports.help = {
    name: 'mywarns',
    usage: 'mywarns',
    type: 'Moderation',
    description: 'Sends you your warns summary as a DM',
};
