const Discord = require('discord.js');
const utils = require('../utils');

module.exports.run = async (bot, message, args) => {
    if (!message.member.hasPermission("MANAGE_MESSAGES")) return;
    var msg;
    var edits;
    if (args[1]) {
        let msgid = args[1];
        let channelid = args[0];
        let channel = await message.guild.channels.cache.get(channelid);
        if (channel.id == channelid) {
            msg = await channel.messages.fetch(msgid);
            if (msg.id == msgid) {
                edits = msg.edits;
            } else return;
        } else return;
    } else {
        let msgid = args[0];
        msg = await message.channel.messages.fetch(msgid);
        if (msg.id == msgid) {
            edits = msg.edits;
        } else return;
    }
    let editarray = [];
    edits.forEach(e => editarray.unshift(` - ${e}`));
    let embed = new Discord.MessageEmbed()
        .setAuthor('Message Edits', msg.author.displayAvatarURL())
        .setDescription(editarray)
        .setTimestamp(msg.editedAt || msg.createdAt)
        .setColor(utils.botColour);
    message.channel.send(embed);
};

module.exports.help = {
    name: 'edits',
    type: 'Moderation',
};
