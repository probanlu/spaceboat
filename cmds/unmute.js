const utils = require('../utils');

module.exports.run = async (bot, message, args) => {
    if (!(message.channel.type === "text")) return;
    console.log("unmuting...");
    const { guild, author } = message;
    // check permission
    if (!message.member.hasPermission("MANAGE_MESSAGES")) return console.log(`${author.username} attempted to unmute without sufficient permissions!`);
    // get mentioned user
    let target = await guild.members.fetch(/^(?:<@!?)?(\d+)>?$/.exec(args[0])[1]); // Get the mentioned user or ID

    // check user mentioned
    if (!target) return console.log(`${author.username} failed to specify a user to unmute!`);
    // search for role
    let role = guild.roles.cache.find(r => r.name === "Muted");
    // unmute
    target.roles.remove(role, `Moderator: ${author.username}`);
    // notify
    await message.channel.send(`${target.user.username} has been unmuted.`);
    utils.logChannel(bot, guild.id, utils.colours.green, `Member unmuted.`, target.user, author);
    console.log(`${target.user.username} has been unmuted.`);
};

module.exports.help = {
    name: 'unmute',
    usage: 'unmute <username>',
    type: 'Moderation',
    description: 'Unmutes the specified user.',
};
