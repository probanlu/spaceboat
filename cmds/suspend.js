const fs = require('fs');
const suspendrolename = require('../data/suspendrolename.json');
const config = require('../data/config.json');

module.exports.run = async (bot, message, args) => {
    if (!(message.channel.type === "text")) return;
    console.log("suspending...");
    const { guild, author } = message;
    //import logChannel.
    // const logChannel = guild.channels.get(guilds[guild.id].logChannelID);   // (not using)
    //check permissions.
    if (!message.member.hasPermission("MANAGE_MESSAGES")) return console.log(`${author.username} attempted to suspend without sufficient permissions!`);
    //import target member from the message.
    let target = await guild.members.fetch(/^(?:<@!?)?(\d+)>?$/.exec(args[0])[1]);
    //breaks if there is no target member.
    if (!target) {
        if (!args[0]) return;
        else if (args[0] == `-config`) {
            // ---------------------------- config ----------------------------
            if (!args[1]) {
                (await message.channel.send(`Please supply the name of  existing role!`)).delete({ timeout: 10000 });
                return;
            } else {
                let role = guild.roles.cache.find(r => r.name === args[1]);
                if (role) {
                    suspendrolename[guild.id] = {
                        roleid: role.id,
                    };
                    fs.writeFileSync("./data/suspendrolename.json", JSON.stringify(suspendrolename, null, 4));
                    return;
                } else {
                    (await message.channel.send(`Please supply the name of an existing role!`)).delete({ timeout: 10000 });
                    return;
                }
            }
        } else return;
    }

    //checks if target is a moderator.
    if (target.hasPermission("MANAGE_MESSAGES")) {
        console.log(`Error: ${target.user.username} is a moderator.`);
        (await message.channel.send(`${target.user.username} is a moderator!`)).delete({ timeout: 10000 });
        return;
    }
    //searches for the role
    try {
        console.log(suspendrolename[guild.id].roleid);
        var role = guild.roles.cache.find(r => r.id == suspendrolename[guild.id].roleid);
    } catch (error) {
        (await message.channel.send(`Woops, no can do, no role! Set up using ${config.prefix}suspend -config`)).delete({ timeout: 10000 });
        return;
    }
    //if no muted role exists...
    // if (!role) {
    //     (await message.channel.send(`Woops, no can do, no role! Set up using ${config.prefix}suspend -config`)).delete(10000);
    //     return;
    // }
    //makes sure that the bot's highest role is above the muted role.
    if (guild.me.roles.highest.comparePositionTo(role) < 1) {
        console.log(`ERROR: Cannot assign suspension role!`);
        return;
    }


    // ---------------------------- UNSUSPEND ----------------------------


    if (target.roles.cache.has(role.id)) {
        console.log("unsuspending....");
        target.roles.remove(role, `Unsuspended by ${author.username}`);
        console.log(`${target.user.username} unsuspended!`);
        (await message.channel.send(`${target.user.username} unsuspended!`)).delete({ timeout: 10000 });
        return;
    } else {


        // ---------------------------- SUSPEND ----------------------------


        if (!args[1]) {
            target.roles.add(role, `Suspended by ${author.username}`);
        } else {
            let reason = args.splice(1).join(' ');
            target.roles.add(role, `Suspended by ${author.username}; "${reason}"`);
        }
    }
};

module.exports.help = {
    name: 'suspend',
    usage: 'suspend <username>',
    type: 'Moderation',
    description: 'Gives user a predefined role',
};
