const fs = require('fs');
const Discord = require('discord.js');
const utils = require('../utils');
const guilds = require('../data/guilds.json');
const bans = require('../data/bans.json');

module.exports.run = async (bot, message, args) => {
    if (!(message.channel.type === "text")) return;
    console.log("banning...");
    const { guild, author } = message;
    //load logChannel
    const logChannel = guild.channels.cache.get(guilds[guild.id].logChannelID);
    //can this user ban members?
    if (!message.member.hasPermission("BAN_MEMBERS")) return console.log(`${author.username} attempted to ban without sufficient permissions!`);
    //Get the mentioned member object
    let target = await guild.members.fetch(/^(?:<@!?)?(\d+)>?$/.exec(args[0])[1]); // Get the mentioned user or ID

    //No member specified?
    if (!target) {
        console.log(`${author.username} failed to specify a target user!`);
        (await message.channel.send(`Please specify a target user.`)).delete({ timeout: 5000 });
        return;
    }
    //Can the target member be banned?
    if (target.hasPermission("MANAGE_MESSAGES")) {
        console.log(`Error: Target user is a moderator.`);
        (await message.channel.send(`${target.user.username} is a moderator!`)).delete({ timeout: 5000 });
        return;
    }

    // THE ACTUAL BAN BEGINS HERE

    // If there are no arguments after the target user is identified
    if (!args[1]) {
        //notify logchannel.
        utils.logChannel(bot, guild.id, utils.colours.red, `Member banned!`, target.user, author)
        //notify channel
        if (utils.thanos.includes(author.id)) { // For the doc
            (await message.channel.send(
                new Discord.MessageEmbed()
                    .setImage('https://media1.tenor.com/images/e36fb32cfc3b63075adf0f1843fdc43a/tenor.gif?itemid=12502580')
                    .setColor(utils.botColour)
                    .setDescription(`${target.user.username} banned! <a:blurpleinfinitygauntlet:537198451188957186>`))
            ).delete({ timeout: 30000 })
            .catch(console.error);
        } else {
            message.channel.send(`${target.user.username} has been banned.`)
            .catch(console.error);
        }
        //notify console.
        console.log(`${target.user.username} has been banned!`);
        //ban the target user.
        guild.members.ban(target, { reason: `Moderator: ${author.username}` });
        return;
    }

    // There are arguments after the user identification.
    let reason;
    let banLength = parseFloat(args[1]);
    // is the first argument a number?
    if (!isNaN(banLength)) {
        // if so, it's a banLength!
        const timeUnit = utils.parseTimeUnit(args[2]);
        // if no time unit supplied, or invalid time unit, fail with message
        if (!timeUnit) {
            (await message.channel.send('Please specify a valid time unit.')).delete({ timeout: 5000 });
            return;
        }
        const [timeUnitName, timeUnitNumMillis] = timeUnit;
        reason = args.splice(3).join(' ');

        let s = 's';
        if (banLength == 1) { s = ''; }

        if (reason) {
            await target.send(`**You have been banned for __${banLength} ${timeUnitName}${s}__ with the following reason:** ${reason}`)
                .catch(console.error);
            // ban the target user
            guild.members.ban(target, { reason: `Moderator: ${author.username}; Reason: ${reason}` });
        }
        if (!reason) {
            reason = '';
            await target.send(`**You have been banned for __${banLength} ${timeUnitName}${s}__**`)
                .catch(console.error);
            // ban the target user
            guild.members.ban(target, { reason: `Moderator: ${author.username}` });
        }
        // since it's a timed ban, create a json entry and write to bans.json
        bans[target.id] = {
            guild: guild.id,
            time: Date.now() + banLength * timeUnitNumMillis,
        };
        fs.writeFileSync("./data/bans.json", JSON.stringify(bans, null, 4));
        await utils.warning(bot, guild.id, target.id, author.id, `**Ban:** ${reason}`, 10, (err, result) => {
            if (err) {
                console.log(err);
                return message.channel.send(`Oops! I didn't manage to correctly log this.`);
            } else {
                // notify channel
                if (utils.thanos.includes(author.id)) { // For the doc
                    message.channel.send(
                        new Discord.MessageEmbed()
                            .setImage('https://media1.tenor.com/images/e36fb32cfc3b63075adf0f1843fdc43a/tenor.gif?itemid=12502580')
                            .setColor(utils.botColour)
                            .setDescription(`${target.user.username} has been banned for ${banLength} ${timeUnitName}${s}.`))
                    .catch(console.error);
                } else {
                    message.channel.send(`${target.user.username} has been banned for ${banLength} ${timeUnitName}${s}.`)
                    .catch(console.error);
                }
                // notify logchannel
                var timeString = `\n**Time:** ${banLength} ${timeUnitName}${s}`
                utils.logChannel(bot, guild.id, utils.colours.red, `Member banned!`, target.user, author, reason, timeString, `\n**Warn ID:** ${result}`);
            }
        })
        // notify console
        console.log(`${target.user.username} has been banned for ${banLength} ${timeUnitName}${s}.`);
    } else {
        reason = args.splice(1).join(' ');
        // notify user
        target.send(`**You have been banned for the following reason:** ${reason}`)
            .catch(console.error);
        // ban.
        guild.members.ban(target, { reason: `Moderator: ${author.username}; Reason: ${reason}` });
        await utils.warning(bot, guild.id, target.id, author.id, `**Ban:** ${reason}`, 10, (err, result) => {
            if (err) {
                console.log(err);
                return message.channel.send(`Oops! I didn't manage to correctly log this.`);
            } else {
                // notify channel
                if (utils.thanos.includes(author.id)) { // For the doc
                    message.channel.send(
                        new Discord.MessageEmbed()
                            .setImage('https://media1.tenor.com/images/e36fb32cfc3b63075adf0f1843fdc43a/tenor.gif?itemid=12502580')
                            .setColor(utils.botColour)
                            .setDescription(`${target.user.username} banned! <a:blurpleinfinitygauntlet:537198451188957186>`))
                    .catch(console.error);
                } else {
                    message.channel.send(`${target.user.username} has been banned.`)
                        .catch(console.error);
                }
                // notify logchannel
                utils.logChannel(bot, guild.id, utils.colours.red, `Member banned!`, target.user, author, reason, '', `\n**Warn ID:** ${result}`);
            }
        })
        // notify console
        console.log(`${target.user.username} has been banned.`);
    }
};

module.exports.help = {
    name: 'ban',
    usage: 'ban <username> [duration] [reason]',
    type: 'Moderation',
    description: 'bans the specified user, with an optional duration and an optional reason.',
};
