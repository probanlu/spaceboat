const fs = require('fs');
const utils = require('../utils');
const guilds = require('../data/guilds.json');
const mutes = require('../data/mutes.json');

module.exports.run = async (bot, message, args) => {
    if (!(message.channel.type === "text")) return;
    console.log("muting...");
    const { guild, author } = message
    //import logChannel.
    const logChannel = guild.channels.cache.get(guilds[guild.id].logChannelID);
    //check permissions.
    if (!message.member.hasPermission("MANAGE_MESSAGES")) return console.log(`${author.username} attempted to mute without sufficient permissions!`);
    let target = await guild.members.fetch(/^(?:<@!?)?(\d+)>?$/.exec(args[0])[1]); // Get the mentioned user or ID

    //breaks if there is no target member.
    if (!target) return console.log(`${author.username} failed to specify a user to mute!`);
    //checks if target is a moderator.
    if (target.hasPermission("MANAGE_MESSAGES")) {
        console.log(`Error: ${target.user.username} is a moderator.`);
        (await message.channel.send(`${target.user.username} is a moderator!`)).delete({ timeout: 10000 });
        return;
    }
    //searches for the role
    let role = guild.roles.cache.find(r => r.name === "Muted");
    //if no muted role exists, do not create it.
    if (!role) {
        (await message.channel.send('Error, there is no muted role (i.e. role named "Muted")')).delete({ timeout: 10000 });
        return;
    }
    //makes sure that the bot's highest role is above the muted role.
    if (guild.me.roles.highest.comparePositionTo(role) < 1) {
        console.log(`ERROR: Cannot assign Muted role!`);
        try {
            logChannel.send(`ERROR: Cannot assign Muted role!`);
        }
        catch (error) {
            console.log('No logchannel defined for this guild!');
            (await message.channel.send('Please configure a logging channel!')).delete({ timeout: 10000 });
        }
        return;
    }
    //checks if member already muted.
    if (target.roles.cache.has(role.id)) {
        console.log(`${target.user.username} already muted!`);
        (await message.channel.send(`${target.user.username} already muted!`)).delete({ timeout: 10000 });
        return;
    }

    // THE ACTUAL MUTE BEGINS HERE

    // There are no arguments after the target user is identified
    if (!args[1]) {
        //notify logchannel.
        utils.logChannel(bot, guild.id, utils.colours.red, `Member muted!`, target.user, author);
        //notify channel
        message.channel.send(`${target.user.username} has been muted.`);
        //notify console.
        console.log(`${target.user.username} has been muted.`);
        //mute the target user.
        await target.roles.add(role, `Moderator: ${author.username}`).catch(err => { console.error(err) });
        return;
    }

    // There are arguments after the user identification.
    let reason;
    let muteLength = parseFloat(args[1]);
    // is the first argument a number?
    if (!isNaN(muteLength)) {
        // if so, it's a muteLength!
        const timeUnit = utils.parseTimeUnit(args[2]);
        // if no time unit supplied, or invalid time unit, fail with message
        if (!timeUnit) {
            (await message.channel.send('Please specify a valid time unit.')).delete({ timeout: 5000 });
            return;
        }
        const [timeUnitName, timeUnitNumMillis] = timeUnit;
        reason = args.splice(3).join(' ');

        let s = 's';
        if (muteLength == 1) { s = ''; }

        if (reason) {
            target.send(`**You have been muted for __${muteLength}__ ${timeUnitName}${s} the following reason:** ${reason}`)
                .catch(console.error);
            // mute the target user
            await target.roles.add(role, `Moderator: ${author.username}; Reason: ${reason}`).catch(err => { console.error(err) });
        }
        if (!reason) {
            reason = '';
            // mute the target user
            await target.roles.add(role, `Moderator: ${author.username}`).catch(err => { console.error(err) });
        }
        // since it's a timed mute, create a json entry and write to mutes.json
        mutes[target.id] = {
            guild: guild.id,
            time: Date.now() + muteLength * timeUnitNumMillis,
        };
        fs.writeFileSync("./data/mutes.json", JSON.stringify(mutes, null, 4));
        // log as warning.
        await utils.warning(bot, guild.id, target.id, author.id, `**Mute:** ${reason}`, 3, (err, result) => {
            if (err) {
                console.log(err);
                return message.channel.send(`Oops! I didn't manage to correctly log this.`);
            } else {
                // notify channel
                message.channel.send(`${target.user.username} has been muted for ${muteLength} ${timeUnitName}${s}.`);
                // notify logchannel
                var timeString = `\n**Time:** ${muteLength} ${timeUnitName}${s}`
                utils.logChannel(bot, guild.id, utils.colours.red, `Member muted!`, target.user, author, reason, timeString, `\n**Warn ID:** ${result}`);
            }
        });
        // notify console
        console.log(`${target.user.username} has been muted for ${muteLength} ${timeUnitName}${s}.`);
    } else {
        let reason = args.splice(1).join(' ');
        // notify user
        target.send(`**You have been muted for the following reason:** ${reason}`)
            .catch(console.error);
        // apply the muted role.
        await target.roles.add(role, `Moderator: ${author.username}. Reason: ${reason}`).catch(err => { console.error(err) });
        // log as warning.
        await utils.warning(bot, guild.id, target.id, author.id, `**Mute:** ${reason}`, 3, (err, result) => {
            if (err) {
                console.log(err);
                return message.channel.send(`Oops! I didn't manage to correctly log this.`);
            } else {
                // notify channel
                message.channel.send(`${target.user.username} has been muted`);
                // notify logchannel
                utils.logChannel(bot, guild.id, utils.colours.red, `Member muted!`, target.user, author, reason, '', `\n**Warn ID:** ${result}`);
            }
        });
        // notify console
        console.log(`${target.user.username} has been muted.`);
    }
};

module.exports.help = {
    name: 'mute',
    usage: 'mute <username> [duration]',
    type: 'Moderation',
    description: 'Mutes the specified user, with an optional duration.',
};
