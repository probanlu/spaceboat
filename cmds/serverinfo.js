// Mostly stolen directly from https://github.com/RayzrDev/SharpBot seems a waste to not put it to at least some good use ;-D
const dateFormat = require('dateformat');
const MessageEmbed = require('discord.js').MessageEmbed;
const utils = require('../utils');

const now = new Date();
dateFormat(now, 'dddd, mmmm dS, yyyy, h:MM:ss TT');

exports.run = async (bot, message, args) => {
    const { guild, author } = message
    if (!guild) {
        throw 'This can only be used in a guild!';
    }

    const millis = new Date().getTime() - guild.createdAt.getTime();
    const days = millis / 1000 / 60 / 60 / 24;

    const owner = guild.owner.user || {};

    const verificationLevels = ['None ,(^.^),', 'Low ┬─┬ ノ( ゜-゜ノ)', 'Medium ヽ(ຈل͜ຈ)ﾉ︵ ┻━┻ ', 'High (╯°□°）╯︵ ┻━┻', 'Extreme ┻━┻彡 ヽ(ಠ益ಠ)ノ彡┻━┻'];

    let embed = new MessageEmbed()
        .setThumbnail(guild.iconURL())
        .setFooter(`requested by ${author.username}#${author.discriminator}`, author.avatarURL())
        .setColor(utils.botColour)
        .addField('Server Name', guild.name, true)
        .addField('Server ID', guild.id, true)
        .addField('Owner', `${owner.username + "#" + owner.discriminator || '� Owner not found...'}`, true)
        .addField('Owner ID', `${owner.id || '� Owner not found...'}`, true)
        .addField('Created On', `${dateFormat(guild.createdAt)}`, true)
        .addField('Days Since Creation', `${days.toFixed(0)}`, true)
        .addField('Region', `${guild.region}`, true)
        .addField('Verification Level',`${verificationLevels[guild.verificationLevel]}`, true)
        .addField('Text Channels', `${guild.channels.cache.filter(m => m.type === 'text').size}`, true)
        .addField('Voice Channels',  `${guild.channels.cache.filter(m => m.type === 'voice').size}`, true)
        .addField('Member Count', `${guild.members.cache.filter(m => m.presence.status !== 'offline').size} / ${guild.memberCount}`, true)
        .addField('Roles', `${guild.roles.cache.size}`, true);

    if (guild.emojis) {
        var emojis = [];
        guild.emojis.cache.forEach(i => {emojis.push(`${i}`)});
        console.log(emojis);

        const numEmojis = guild.emojis.cache.size;
        if (emojis.length < 25) { // In other words, if there is enough space in one field to fit them all
            const emojiStr = emojis.join(''); // Unless the emoji names are on average longer than 18 characters, this will work.
            console.log(emojiStr.length);
            if (emojiStr.length<1014) embed.addField('Emojis', `${emojiStr || 'None'} \nThat's ${numEmojis} emojis.`, false);
            else return; // If you're insane with your emoji names, I won't list them!
        } else {
            if (emojis.length < 50) {
                let emojis1 = emojis.slice(0,24).join('');
                let emojis2 = emojis.slice(25).join('');
                if (emojis1.length<1024 && emojis2.length<1014) {
                    embed.addField('Emojis', `${emojis1 || 'None'}`, false);
                    embed.addField('Emojis (cont)', `${emojis2 || 'None'} \nThat's ${numEmojis} emojis.`, false);
                } else return;
            } else if (emojis.length < 75) {
                const emojis1 = emojis.slice(0,24).join('');
                const emojis2 = emojis.slice(25,49).join('');
                let emojis3 = emojis.slice(50).join('');
                if (emojis1.length<1024 && emojis2.length<1024 && emojis3.length<1014) {
                    embed.addField('Emojis', `${emojis1 || 'None'}`, false);
                    embed.addField('Emojis (cont)', `${emojis2 || 'None'}`, false);
                    embed.addField('Emojis (continued)', `${emojis3 || 'None'} \nThat's ${numEmojis} emojis.`, false);
                } else return;
            } else if (emojis.length < 100) {
                const emojis1 = emojis.slice(0,24).join('');
                const emojis2 = emojis.slice(25,49).join('');
                const emojis3 = emojis.slice(50,74).join('');
                let emojis4 = emojis.slice(75).join('');
                if(emojis1.length<1024 && emojis2.length<1024 && emojis3.length<1024 && emojis4.length<1014) {
                    embed.addField('Emojis', `${emojis1 || 'None'}`, false);
                    embed.addField('Emojis (cont)', `${emojis2 || 'None'}`, false);
                    embed.addField('Emojis (continued)', `${emojis3 || 'None'}`, false);
                    embed.addField('Emojis (continued) (by the way you\'re friggen mad.)', `${emojis4 || 'None'} \nThat's ${numEmojis} emojis.`, false);
                } else return;
            } else if (emojis.length < 125) {
                const emojis1 = emojis.slice(0,24).join('');
                const emojis2 = emojis.slice(25,49).join('');
                const emojis3 = emojis.slice(50,74).join('');
                const emojis4 = emojis.slice(75,99).join('');
                let emojis5 = emojis.slice(100).join('');
                if (emojis1.length<1024 && emojis2.length<1024 && emojis3.length<1024 && emojis4.length<1024 && emojis5.length<1014) {
                    embed.addField('Emojis', `${emojis1 || 'None'}`, false);
                    embed.addField('Emojis (cont)', `${emojis2 || 'None'}`, false);
                    embed.addField('Emojis (continued)', `${emojis3 || 'None'}`, false);
                    embed.addField('Emojis (continued) (by the way you\'re friggen mad.)', `${emojis4 || 'None'}`, false);
                    embed.addField('Emojis (continued) (why. why do you have so many)', `${emojis5 || 'None'} \nThat's ${numEmojis} emojis.`, false);
                } else return;
            } else {
                let response = utils.randomSelection([
                    `Nope. Too many.`,
                    `I refuse to list these.`,
                    `You have way. Too. Many. Emojis.`,
                    `...`,
                    'why?',
                    'No. Just no. You\'ve got way too many.',
                    'nope',
                    '<:rauf:427621360286826496>',
                    '<:thenk:427621426514755601>',
                    '<:oh:427621570320531456>',
                    '<:waitwhat:427621591036198914>',
                    '<a:gone:427621026957099009>',
                    '<:ohno:427621654684893194>',
                    '<:thohno:427621998089338880>'
                ])
                embed.addField('Emojis', response + ` There's like... ${numEmojis} emojis.`, false);
            }
        }
    }
    if (guild.features.join(', ')) embed.addField('Features', `${guild.features.join(', ') || 'None'}`);
    console.log(guild.features);
    message.channel.send(embed);
};

module.exports.help = {
    name: 'serverinfo',
    usage: 'serverinfo',
    description: 'Shows info of the server you are in',
};
